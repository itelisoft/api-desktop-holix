<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {Schema::create('alumnos', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nombre');
        $table->string('edad');
        $table->string('codigo')->unique();
        $table->string('estatura');
        $table->string('complexion');
        $table->string('peso');
        $table->mediumText('notas');
        $table->string('genero');
        $table->string('id_gimnasios');
        $table->string('id_rutina');
        $table->string('id_entrenadores');
        $table->rememberToken();
        $table->timestamps();
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
