<?php

namespace App\Http\Controllers;

use App\Asistencias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AsistenciasController extends Controller
{

    public function storeAsistencias(Request $request)
    {

        $data = $request->json()->all();

        $asistencia = new Asistencias();
        $asistencia->id_socio = $data['id_socio'];
        $asistencia->save();

        $array = array(
            "id" => $asistencia->id,
            "id_socio" => $asistencia->id_socio
        );
        return $array;
    }

    public function consultarAsistencia($id)
    {
        $consultar = Asistencias::where('id', $id)->get();
        return response($consultar);
    }


    public function consultarAsistencias()
    {
        return response(Asistencias::all());

    }


    public function buscarAsistencia($search)
    {

        $users = DB::table('asistencias')
            ->Where('id_socio','LIKE',"%{$search}%")
            ->get();
        return $users;

    }


    public function actualizarAsistencia(Request $request)
    {
        $data = $request->json()->all();

        $actualizarDatos = Asistencias::find($data['id']);
        $actualizarDatos->id_socio = $data['id_socio'];
        $actualizarDatos->save();
        $array = array(
            "id" => $actualizarDatos->id,
            "id_socio" => $actualizarDatos->id_socio
        );
        return $array;
    }

    public function  eliminarAsistencia($id){
        $Elimar=Asistencias::where('id',$id)->delete();
        $array = array(
            "eliminado" => "la asistencia a sido eliminada con exito",

        );

        return $array;

    }

    public  function  busquedaFecha(Request $request,$search){
        $users=  DB::table('asistencias')
            ->leftJoin('socios', 'socios.id', '=', 'asistencias.id_socio')
            ->Where('asistencias.created_at','LIKE',"%{$search}%")
            ->select('asistencias.created_at','socios.nombre','socios.codigo')
            ->get();

        return $users;
    }

    public function date(Request $request, $inicio , $fin){

        $users=  DB::table('asistencias')
            ->leftJoin('socios', 'socios.id', '=', 'asistencias.id_socio')
            ->where('asistencias.created_at','>=',$inicio)->where('asistencias.created_at','<=',$fin)
            ->select('asistencias.created_at','socios.nombre','socios.codigo')
            ->get();


      // $users = Asistencias::where('created_at','>=',$inicio)->where('created_at','<=',$fin)->get();

return $users;
    }
}
