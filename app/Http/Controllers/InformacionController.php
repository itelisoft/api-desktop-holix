<?php

namespace App\Http\Controllers;

use App\Informacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InformacionController extends Controller
{
    public function storeInformacion(Request $request)
    {

        $data = $request->json()->all();

        $informacion = new Informacion();
        $informacion->nombre = $data['nombre'];
        $informacion->url = $data['url'];
        $informacion->configuracion = $data['configuracion'];
        $informacion->saldo_caja = $data['saldo_caja'];
        $informacion->save();

        $array = array(
            "id" => $informacion->id,
            "nombre" => $informacion->nombre,
            "url" => $informacion->url,
            "configuracion" => $informacion->configuracion,
            "saldo_caja" => $informacion->saldo_caja
        );
        return $array;
    }

    public function consultaInformacion($id)
    {
        $consultar = Informacion::where('id', $id)->get();
        return response($consultar);
    }


    public function consultarInformacion()
    {
        return response(Informacion::all());

    }


    public function buscarporNombre($search)
    {

        $users = DB::table('informacion')
            ->Where('nombre','LIKE',"%{$search}%")
            ->get();
        return $users;

    }


    public function actualizarInformacion(Request $request)
    {
        $data = $request->json()->all();

        $actualizarDatos = Informacion::find($data['id']);
        $actualizarDatos->nombre = $data['nombre'];
        $actualizarDatos->url = $data['url'];
        $actualizarDatos->configuracion = $data['configuracion'];
        $actualizarDatos->saldo_caja = $data['saldo_caja'];
        $actualizarDatos->save();
        $array = array(
            "id" => $actualizarDatos->id,
            "nombre" => $actualizarDatos->nombre,
            "url" => $actualizarDatos->url,
            "configuracion" => $actualizarDatos->configuracion,
            "saldo_caja" => $actualizarDatos->saldo_caja
        );
        return $array;

    }

    public function  eliminarInformacion($id){
        $Elimar=Informacion::where('id',$id)->delete();
        $array = array(
            "eliminado" => "la informacion a sido eliminada con exito",

        );

        return $array;

    }


    public function actualizarConfiguracion(Request $request)
    {
        $data = $request->json()->all();

        $actualizarDatos = Informacion::find($data['id']);
        $actualizarDatos->configuracion = $data['configuracion'];
        $actualizarDatos->saldo_caja = $data['saldo_caja'];
        $actualizarDatos->save();
        $array = array(
            "id" => $actualizarDatos->id,
            "configuracion" => $actualizarDatos->configuracion,
            "saldo_caja" => $actualizarDatos->saldo_caja,
            );
        return $array;

    }

    public  function  actualizarUrl(Request $request){
        $data = $request->json()->all();

        $actualizarDatos = Informacion::find($data['id']);
        $actualizarDatos->nombre = $data['nombre'];
        $actualizarDatos->url = $data['url'];
        $actualizarDatos->save();
        $array = array(
            "id" => $actualizarDatos->id,
            "nombre" => $actualizarDatos->nombre,
            "url" => $actualizarDatos->url
        );
        return $array;


    }
}
