<?php

namespace App\Http\Controllers;

use App\Caja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CajaController extends Controller
{
    public function storeCaja(Request $request)
    {

        $data = $request->json()->all();

        $caja = new Caja();
        $caja->cantidad = $data['cantidad'];
        $caja->tipo = $data['tipo'];
        $caja->fecha = $data['fecha'];
        $caja->cantidad_retirada = $data['cantidad_retirada'];
        $caja->cantidad_restante = $data['cantidad_restante'];
        $caja->notas = $data['notas'];
        $caja->id_empleado = $data['id_empleado'];
        $caja->save();

        $array = array(
            "id" => $caja->id,
            "cantidad" => $caja->cantidad,
            "tipo" => $caja->tipo,
            "fecha" => $caja->fecha,
            "cantidad_retirada" => $caja->cantidad_retirada,
            "cantidad_restante" => $caja->cantidad_restante,
            "notas" => $caja->notas,
            "id_empleado" => $caja->id_empleado
        );
        return $array;
    }

    public function consultarCaja($id)
    {
        $consultar = Caja::where('id', $id)->get();
        return response($consultar);
    }


    public function consultarCajas()
    {
        return response(Caja::all());

    }


    public function buscar($search)
    {

        $users = DB::table('caja')
            ->Where('tipo','LIKE',"%{$search}%")
            ->get();
        return $users;

    }




    public function ActualizarCaja(Request $request)
    {
        $data = $request->json()->all();

        $actualizarDatos = Caja::find($data['id']);
        $actualizarDatos->cantidad = $data['cantidad'];
        $actualizarDatos->tipo = $data['tipo'];
        $actualizarDatos->fecha = $data['fecha'];
        $actualizarDatos->cantidad_retirada = $data['cantidad_retirada'];
        $actualizarDatos->cantidad_restante = $data['cantidad_restante'];
        $actualizarDatos->notas = $data['notas'];
        $actualizarDatos->id_empleado = $data['id_empleado'];
        $actualizarDatos->save();
        $array = array(
            "id" => $actualizarDatos->id,
            "cantidad" => $actualizarDatos->cantidad,
            "tipo" => $actualizarDatos->tipo,
            "fecha" => $actualizarDatos->fecha,
            "cantidad_retirada" => $actualizarDatos->cantidad_retirada,
            "cantidad_restante" => $actualizarDatos->cantidad_restante,
            "notas" => $actualizarDatos->notas,
            "id_empleado" => $actualizarDatos->id_empleado

        );
        return $array;

    }

    public function  eliminarCaja($id){
        $Elimar=Caja::where('id',$id)->delete();
        $array = array(
            "eliminado" => "el monto a sido eliminado con exito",

        );

        return $array;

    }

    public function buscaFecha($search)
    {

       /* $users = DB::table('caja')
            ->Where('fecha','LIKE',"%{$search}%")
            ->get();
        return $users;*/

        $users=  DB::table('empleados')
            ->leftJoin('caja', 'empleados.id', '=', 'caja.id_empleado')
            ->Where('caja.fecha','LIKE',"%{$search}%")
            ->select('empleados.nombre','caja.cantidad','caja.tipo','caja.fecha','caja.cantidad_retirada','caja.cantidad_restante','caja.notas','caja.id_empleado')
            ->get();

        return $users;

    }

    public  function  dosFechas(Request $request,$inicio ,$fin){

     /*   $caja = Caja::whereBetween('fecha', array($inicio, $fin))
            ->orderBy('fecha', 'asc')
            ->get();

        return $caja;
*/

        $users=  DB::table('empleados')
            ->leftJoin('caja', 'empleados.id', '=', 'caja.id_empleado')
            ->whereBetween('fecha', array($inicio, $fin))
            ->select('empleados.nombre','caja.cantidad','caja.tipo','caja.fecha','caja.cantidad_retirada','caja.cantidad_restante','caja.notas','caja.id_empleado')
            ->orderBy('caja.fecha', 'asc')
            ->get();

        return $users;
    }
}
