<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MensajeController extends Controller
{
    public function mensaje(Request $request,$nombre,$correo,$codigo){

    ini_set( 'display_errors', 1 );
     error_reporting( E_ALL );
     $from = "itelisoft@gmail.com";
     $to = "adrianlic@me.com";
     $subject = "REGISTRO SPORT GYM HOLIX";
     $message = "Se registró el usuario: ".$nombre." con el correo: ".$correo."  y con el código: ".$codigo;
     $headers = "From:" . $from;
     mail($to,$subject,$message, $headers);
     echo "The email message was sent.";
    }

    public function mensajePrueba(Request $request,$nombre,$correo,$codigo){

        ini_set( 'display_errors', 1 );
        error_reporting( E_ALL );
        $from = "itelisoft@gmail.com";
        $to = "marilolidollypp@gmail.com";
        $subject = "REGISTRO SPORT GYM HOLIX";
        $message = "Se registro el usuario: ".$nombre." con el correo: ".$correo."  y con el código: ".$codigo;
        $headers = "From:" . $from;
        mail($to,$subject,$message, $headers);
        echo "The email message was sent.";
    }

    public function mensajeUsuario(Request $request,$correo,$codigo){

        ini_set( 'display_errors', 1 );
        error_reporting( E_ALL );
        $from = "holixlabsportgym@gmail.com";
        $to = $correo;
        $subject = "REGISTRO SPORT GYM HOLIX";
        $message = "Bienvenido a Sport Gym Holix , tu código es: ".$codigo;
        $headers = "From:" . $from;
        mail($to,$subject,$message, $headers);
        echo "The email message was sent.";
    }
}
