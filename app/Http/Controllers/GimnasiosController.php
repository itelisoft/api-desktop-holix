<?php

namespace App\Http\Controllers;
use App\Gimnasios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GimnasiosController extends Controller
{
    public function login(Request $request)
    {

        $data = $request->json()->all();

        $infocodigo = Gimnasios::where('codigo', '=', $data['codigo'])->get();


        if (count($infocodigo) == 0) {
            return $array = array(
                "id"   =>  "null"
            );
        }



        $array = array(
            "id"   =>  $infocodigo[0]->id
        );

        return $infocodigo[0];

    }



    public function store(Request $request){

        $gym = new Gimnasios();
        $gym->nombre =$request->input('nombre');
        $gym->codigo = $request->input('codigo');
        $gym->activo =' activar';
        $gym->logo =$request->input('logo');
        $gym->save();



      flash('Los datos se guaradaron con exito!')->success();

     return redirect('view/gimnasios/agregaGym');
   //     return $gym->id;
    }



    public function  EliminarGym($id){
        $Elimar=Gimnasios::where('id',$id)->delete();
        return "El usuario a sido eliminado con éxito!";
    }



    public function consultarGym($id)
    {
        $inv = Gimnasios::where('id', $id)->get();
        return response($inv);
    }




    public function consultarGyms()
    {
        return response(Gimnasios::all());
    }


    public function GymaActivo($id)
    {

        $confirmar = Invitados::find($id);

        $confirmar->confirmacion_asistencia = 'ACTIVO';

        $confirmar->save();
        return "usted a cancelado su asistencia";


    }


    public function GymDesactivado($id)
    {

        $confirmar = Invitados::find($id);

        $confirmar->confirmacion_asistencia = 'DESACTIVADO';

        $confirmar->save();
        return "usted a cancelado su asistencia";


    }


    public function agregaGym()
    {
        if (Auth::check()) {
            return view('gimnasio.agregarGym');

        }else{
            return redirect('login');
        }

    }

    public function ShowGym()
    {
        if (Auth::check()) {
            return view('gimnasio.showGym');

        }else{
            return redirect('login');
        }

    }
}
