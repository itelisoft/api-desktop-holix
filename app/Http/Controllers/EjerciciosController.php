<?php

namespace App\Http\Controllers;
use App\Ejercicios;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class EjerciciosController extends Controller
{
    public function store(Request $request){

        $data = $request->json()->all();

        $ejercicio = new Ejercicios();
        $ejercicio->plantilla_ejer_id =$request->input('plantilla_ejer_id');
        $ejercicio->rutina_id = $request->input('rutina_id');
        $ejercicio->notas =$request->input('notas');
        $ejercicio->dia =$request->input('dia');
        $ejercicio->save();

        return $ejercicio->id;
    }

    public function storeEjercicio(Request $request,$id){



        DB::table('ejercicios')->insert([
            ['plantilla_ejer_id' => '116', 'rutina_id' => " ' ".$id." ' " ,'notas' => '4 series - 8 repeticiones', 'dia' => 'Lunes'],
            ['plantilla_ejer_id' => '111', 'rutina_id' => '284','notas' => '4 series - 8 repeticiones', 'dia' => 'Lunes'],
            ['plantilla_ejer_id' => '112', 'rutina_id' => '284','notas' => '4 series - 8 repeticiones', 'dia' => 'Lunes'],
            ['plantilla_ejer_id' => '113', 'rutina_id' => '284','notas' => '4 series - 8 repeticiones', 'dia' => 'Lunes'],
            ['plantilla_ejer_id' => '63', 'rutina_id' => '284','notas' => '4 series - 8 repeticiones', 'dia' => 'Lunes']
        ]);
    }


    public function  eliminarEjercicio($id){
        $Elimar=Ejercicios::where('id',$id)->delete();
        return "El ejercicio a sido eliminado con éxito!";
    }


    public function consultarEjercicio($id)
    {
        $inv = Ejercicios::where('id', $id)->get();
        return response($inv);
    }



    public function consultarEjercicios()
    {
        $users = DB::table('ejercicios')
            ->orderBy('plantilla_ejer_id', 'asc')
            ->orderBy('rutina_id', 'asc')
            ->get();

        return $users;
    }



    public function consultarEjerciciosByRutinaId($rutina_id)
    {
        $ejercicios = Ejercicios::where('rutina_id', $rutina_id)->get();
        return response($ejercicios);
    }



    public function actualizarid_ejercicio_id_rutina($id,$plantilla_ejer_id,$rutina_id)
    {

        $actualizarDatos = Ejercicios::find($id);

        $actualizarDatos ->plantilla_ejer_id = $plantilla_ejer_id;
        $actualizarDatos ->rutina_id = $rutina_id;
        $actualizarDatos->save();
        return "se actualizarón los datos ";


    }


    public function actualizarEjercicio($id,$rutina_id)
    {

        $actualizarDatos = Ejercicios::find($id);

      //  $actualizarDatos ->plantilla_ejer_id = $plantilla_ejer_id;
        $actualizarDatos ->rutina_id = $rutina_id;
        $actualizarDatos->save();
        return "se actualizarón los datos ";


    }

    // -----------------------------------------------------------------------------------------------------------------

    public function consultarEjer($rutina_id)
    {
       return $rutina = Ejercicios::where('rutina_id', $rutina_id)->get(['id','notas','plantilla_ejer_id']);

    }

    public function consultaridplantilla($rutina_id)
    {
        return Ejercicios::where('rutina_id', $rutina_id)->pluck('plantilla_ejer_id');

    }

    public function contador($rutina_id)
    {
    $consulta= Ejercicios::where('rutina_id', $rutina_id)->pluck('plantilla_ejer_id');
    //$contar = $consulta-1;
    echo count($consulta)-1;


    }




    // -----------------------------------------------------------------------------------------------------------------






}
