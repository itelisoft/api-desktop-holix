<?php

namespace App\Http\Controllers;
use App\Alumnos;
use App\Entrenadores;

use App\Rutinas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlumnosController extends Controller
{
    public function login(Request $request)
    {

        $data = $request->json()->all();

        $infocodigo = Alumnos::where('codigo', '=', $data['codigo'])->get();


        if (count($infocodigo) == 0) {
            return "CÓDIGO NO REGISTRADO";
        }

       // return $infocodigo[0]->id;
      //   $infocodigo[0]->id." , ".$infocodigo[0]->nombre." , ".$infocodigo[0]->id_gimnasios;

      $array = array(
            "id" => $infocodigo[0]->id,
            "nombre" => $infocodigo[0]->nombre,
            "id_gimnasios" => $infocodigo[0]->id_gimnasios
        );
        return $array;

    }



    public function store(Request $request){

        $alumno = new Alumnos();
        $alumno->nombre =$request->input('nombre');
        $alumno->edad = $request->input('edad');
        $alumno->codigo =$request->input('codigo');
        $alumno->estatura = $request->input('estatura');
        $alumno->complexion = $request->input('complexion');
        $alumno->peso = $request->input('peso');
        $alumno->notas =$request->input('notas');
        $alumno->id_gimnasios = $request->input('id_gimnasios');
        $alumno->id_rutina ='id';
        $alumno->id_entrenadores = $request->input('id_entrenadores');
        $alumno->save();




            $controller = new RutinasController;
            $controller->crearRutina($alumno->id);

            $actualizarDatos = Alumnos::find($alumno->id);
            $actualizarDatos->id_rutina = $alumno->id;
            $actualizarDatos->save();

            return $alumno->id;

    }

    public function storeAlumno(Request $request){

        $data = $request->json()->all();
        $consultaCodigo =Alumnos::where('codigo', '=', $data['codigo'])->get();



        if(count($consultaCodigo)== 0) {
            $alumno = new Alumnos();
            $alumno->nombre = $data['nombre'];
            $alumno->edad = $data['edad'];
            $alumno->codigo = $data['codigo'];
            $alumno->estatura = $data['estatura'];
            $alumno->complexion = $data['complexion'];
            $alumno->peso = $data['peso'];
            $alumno->notas = $data['notas'];
            $alumno->id_gimnasios = $data['id_gimnasios'];
            $alumno->id_rutina = 'id';
            $alumno->id_entrenadores = $data['id_entrenadores'];

            $alumno->save();

            $controller = new RutinasController;
            $controller->crearRutina($alumno->id);

            $actualizarDatos = Alumnos::find($alumno->id);
            $actualizarDatos->id_rutina = $alumno->id;
            $actualizarDatos->save();

            return $alumno->id;
        } else {


                print 'Digita otro código';

        }



    }

    public function storeJsonHombre(Request $request,$h){

        $data = $request->json()->all();

            $alumno = new Alumnos();
            $alumno->nombre = $data['nombre'];
            $alumno->edad = $data['edad'];
            $alumno->codigo = '';
            $alumno->estatura = $data['estatura'];
            $alumno->complexion = $data['complexion'];
            $alumno->peso = $data['peso'];
            $alumno->notas = $data['notas'];
            $alumno->id_gimnasios = '46';
            $alumno->id_rutina = '951';
            $alumno->id_entrenadores = '103';
            $alumno->save();
        $nombre = explode(" ", $alumno->nombre);


        $actualizarDatos = Alumnos::find($alumno->id);
        $actualizarDatos->codigo = $nombre[0].$alumno->id;
        $actualizarDatos->save();

        $controller = new RutinasController;
        $controller->crearRutinaWeb($alumno->id);



        $array = array(
            "id" => $alumno->id,
            "nombre" => $alumno->nombre,
            "edad" => $alumno->edad,
            "codigo" => $actualizarDatos->codigo,
            "estatura" => $alumno->estatura,
            "complexion" => $alumno->complexion,
            "peso" => $alumno->peso,
            "notas" => $alumno->notas,
            "id_gimnasios" => $alumno->id_gimnasios,
            "id_rutina" => $actualizarDatos->id_rutina,
            "id_entrenadores" => $alumno->id_entrenadores,
            "hombre" => $h

        );





        return $array;

    }

    public function storeJsonMujer(Request $request,$m){

        $data = $request->json()->all();

        $alumno = new Alumnos();
        $alumno->nombre = $data['nombre'];
        $alumno->edad = $data['edad'];
        $alumno->codigo = '';
        $alumno->estatura = $data['estatura'];
        $alumno->complexion = $data['complexion'];
        $alumno->peso = $data['peso'];
        $alumno->notas = $data['notas'];
        $alumno->id_gimnasios = '46';
        $alumno->id_rutina = '952';
        $alumno->id_entrenadores = '103';
        $alumno->save();

        $nombre = explode(" ", $alumno->nombre);

        $actualizarDatos = Alumnos::find($alumno->id);
        $actualizarDatos->codigo = $nombre[0].$alumno->id;
        $actualizarDatos->save();



        $controller = new RutinasController;
        $controller->crearRutinaWeb($alumno->id);



        $array = array(
            "id" => $alumno->id,
            "nombre" => $alumno->nombre,
            "edad" => $alumno->edad,
            "codigo" => $actualizarDatos->codigo,
            "estatura" => $alumno->estatura,
            "complexion" => $alumno->complexion,
            "peso" => $alumno->peso,
            "notas" => $alumno->notas,
            "id_gimnasios" => $alumno->id_gimnasios,
            "id_rutina" => $actualizarDatos->id_rutina,
            "id_entrenadores" => $alumno->id_entrenadores,
            "mujer" => $m

        );
        return $array;

    }

    public function  Eliminaralumno($id){
        $Elimar=Alumnos::where('id',$id)->delete();
        return "El usuario a sido eliminado con éxito!";
    }



    public function consultarAlumno($id)
    {
        $inv = Alumnos::where('id', $id)->get();
        return response($inv);
    }



    public function consultaAlumno($id)
    {
        $inv = Alumnos::where('id', $id)->pluck('id_rutina')[0];
        return response($inv);
    }






    public function consultarAlumnos()
    {

        return response(Alumnos::all('id','nombre'));


    }



    public function buscar_id_rutina($id)
    {
        $users = Alumnos::table('alumno')->select('id_rutina',$id)->get();

        return response($users);

    }

    public  function ordenAlfebetico(){
        $users = DB::table('alumnos')
            ->orderBy('nombre', 'asc')
            ->get();

        return $users;
    }

    public function ActualizarDatosAlumno($id,$nombre,$edad,$codigo,$estatura,$complexion,$peso,$notas)
    {

        $actualizarDatos = Alumnos::find($id);
        $actualizarDatos ->nombre = $nombre;
        $actualizarDatos ->edad = $edad;
        $actualizarDatos ->codigo = $codigo;
        $actualizarDatos ->estatura = $estatura;
        $actualizarDatos ->complexion = $complexion;
        $actualizarDatos ->peso = $peso;
        $actualizarDatos ->notas = $notas;

        $actualizarDatos->save();
        return "se actualizarón los datos del alumno";

    }

    public function ActualizarAlumno(Request $request)
    {

        $data = $request->json()->all();
        $consultaCodigo =Alumnos::where('codigo', '=', $data['codigo'])->get();


        if(count($consultaCodigo)== 0) {

            $actualizarDatos = Alumnos::find($data['id']);
            $actualizarDatos->nombre = $data['nombre'];
            $actualizarDatos->edad = $data['edad'];
            $actualizarDatos->codigo = $data['codigo'];
            $actualizarDatos->estatura = $data['estatura'];
            $actualizarDatos->complexion = $data['complexion'];
            $actualizarDatos->peso = $data['peso'];
            $actualizarDatos->notas = $data['notas'];

            $actualizarDatos->save();
            return "se actualizarón los datos del alumno";
        }
        else{

           if($consultaCodigo[0]->id ==  $data['id']  ){

               $actualizarDatos = Alumnos::find($data['id']);
               $actualizarDatos->nombre = $data['nombre'];
               $actualizarDatos->edad = $data['edad'];
               $actualizarDatos->codigo = $data['codigo'];
               $actualizarDatos->estatura = $data['estatura'];
               $actualizarDatos->complexion = $data['complexion'];
               $actualizarDatos->peso = $data['peso'];
               $actualizarDatos->notas = $data['notas'];

               $actualizarDatos->save();
               return "se actualizo";
           }else{

               return 'Digita otro código';
           }


        }
    }

    public function ConsultarEntrenadorAlumno($id_alumno)
    {
       $inv = Alumnos::where('id', $id_alumno)->pluck('id_entrenadores')[0];
        echo $inv;


    }




    public function alumnotieneEntrenador($id)
    {


      $id_entrenador = $this->ConsultarEntrenadorAlumno($id);

     /* $controller = new EntrenadoresController;
     $respuesta =  $controller->consultarDatosEntrenador($id_entrenador);*/
   return $id_entrenador;
    }

    public function buscarAlumnoporNombre($id_gym,$search)
    {

        $users = DB::table('alumnos')
            ->where('id_gimnasios', $id_gym)
            ->Where('nombre','LIKE',"%{$search}%")
            ->get(['id','nombre']);
        return $users;

    }

    public function buscarAlumnoporCodigo($id_gym,$search)
    {
        $users = DB::table('alumnos')
            ->where('id_gimnasios', $id_gym)
            ->Where('codigo','LIKE',"%{$search}%")
            ->get(['id','nombre','codigo']);
        return $users;


    }



    public function consultaridAlumno($id)
    {
        $inv = Alumnos::where('id', $id)->pluck('id_rutina')[0];
        return $inv;
    }

    public function ListarAlumnos($id_gimnasios)
    {
        return Alumnos::where('id_gimnasios', $id_gimnasios)->get(['id','nombre']);
    }

    public function  RutinaCompletaAlumno($id_rutina){


        $controllerEjercicios = new EjerciciosController;
        $listaEjercicios= $controllerEjercicios->consultarEjer($id_rutina);


 $listaCompleta=array();
        for($i=0;$i<count($listaEjercicios);$i++){

            $controllerPlantillas = new Plantillas_EjerController;
            $plantila= collect($controllerPlantillas->consultarDatos($listaEjercicios[$i]['plantilla_ejer_id']))->toArray();

       return  $plantila[0][0];

          /*  $post['notas'] =   $listaEjercicios[$i]['notas'];
              $post['nombre'] =  $plantila['nombre'];
              $post['descripcion'] =  $plantila['descripcion']; */

         //array_push($listaCompleta, $post);

        }}

//print_r($listaCompleta);




        public function buscaAlumosDeEntrenador($search)
        {
            $users = DB::table('alumnos')
                ->Where('id_entrenadores','LIKE',"%{$search}%")
                ->orderBy('nombre', 'asc')
                ->get();
            return $users;


        }





        public  function fechas($inicio,$fin){

            $users=  DB::table('alumnos')
                ->whereBetween('created_at', array($inicio, $fin))
                ->orderBy('created_at', 'asc')
                ->get();

            return $users;


        }





}
