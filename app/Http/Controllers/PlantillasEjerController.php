<?php

namespace App\Http\Controllers;

use App\Plantillas_Ejer;
use Illuminate\Http\Request;
use App\Plantillasejer;
class PlantillasEjerController extends Controller
{
    public function store(Request $request){

        $platilla = new Plantillasejer();
        $platilla->nombre =$request->input('nombre');
        $platilla->descripcion = $request->input('descripcion');
        $platilla->save();

        return $platilla->id;
    }

    public function  EliminarPlanEjer($id){
        $Elimar=Plantillasejer::where('id',$id)->delete();
        return "La plantilla a sido eliminado con éxito!";
    }



    public function consultarPlaejer($id)
    {
        $inv = Plantillasejer::where('id', $id)->get();
        return response($inv);
    }




    public function consultarPlanEjercicios()
    {
        return response(Plantillasejer::all());
    }

    public function buscarPlantilla($search)
    {
        return  $user = Plantillas_Ejer::where('nombre','LIKE',"%{$search}%")->get(['id','nombre']);
    }
}
