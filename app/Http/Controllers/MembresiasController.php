<?php

namespace App\Http\Controllers;

use App\Membresias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MembresiasController extends Controller
{
    public function storeMembresias(Request $request)
    {

        $data = $request->json()->all();

        $productos = new Membresias();
        $productos->nombre = $data['nombre'];
        $productos->duracion = $data['duracion'];
        $productos->costo = $data['costo'];
        $productos->save();

        $array = array(
            "id" => $productos->id,
            "nombre" => $productos->nombre,
            "duracion" => $productos->duracion,
            "costo" => $productos->costo
        );
        return $array;
    }

    public function consultarMembresia($id)
    {
        $consultar = Membresias::where('id', $id)->get();
        return response($consultar);
    }


    public function consultarMembresias()
    {
        return response(Membresias::all());

    }


    public function buscarmembresiaporNombre($search)
    {

        $users = DB::table('membresias')
            ->Where('nombre','LIKE',"%{$search}%")
            ->get();
        return $users;

    }


    public function actualizarMembresia(Request $request)
    {
        $data = $request->json()->all();

        $actualizarDatos = Membresias::find($data['id']);
        $actualizarDatos->nombre = $data['nombre'];
        $actualizarDatos->duracion = $data['duracion'];
        $actualizarDatos->costo = $data['costo'];
        $actualizarDatos->save();
        $array = array(
            "id" => $actualizarDatos->id,
            "nombre" => $actualizarDatos->nombre,
            "duracion" => $actualizarDatos->duracion,
            "costo" => $actualizarDatos->costo
        );
        return $array;

    }

    public function  eliminarMembresia($id){
        $Elimar=Membresias::where('id',$id)->delete();
        $array = array(
            "eliminado" => "la membresia a sido eliminada con exito",

        );

        return $array;

    }
}
