<?php

namespace App\Http\Controllers;

use App\Movimientos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MovimientosController extends Controller
{
    public function storeMovimientos(Request $request)
    {

        $data = $request->json()->all();

        $movimiento = new Movimientos();
        $movimiento->cantidad = $data['cantidad'];
        $movimiento->descripcion = $data['descripcion'];
        $movimiento->id_empleado = $data['id_empleado'];
        $movimiento->id_caja = $data['id_caja'];
        $movimiento->save();

        $array = array(
            "id" => $movimiento->id,
            "cantidad" => $movimiento->cantidad,
            "descripcion" => $movimiento->descripcion,
            "id_empleado" => $movimiento->id_empleado,
            "id_caja" => $movimiento->id_caja
        );
        return $array;
    }

    public function consultarMovimiento($id)
    {
        $consultar = Movimientos::where('id', $id)->get();
        return response($consultar);
    }


    public function consultarmovimientos()
    {
        return response(Movimientos::all());

    }


    public function buscarEmpleadoporDescrip($search)
    {

        $users = DB::table('movimientos')
            ->Where('descripcion','LIKE',"%{$search}%")
            ->get();
        return $users;

    }


    public function actualizarMovimiento(Request $request)
    {
            $data = $request->json()->all();

            $actualizarDatos = Movimientos::find($data['id']);
            $actualizarDatos->cantidad = $data['cantidad'];
            $actualizarDatos->descripcion = $data['descripcion'];
            $actualizarDatos->id_empleado = $data['id_empleado'];
            $actualizarDatos->id_caja = $data['id_caja'];
            $actualizarDatos->save();
        $array = array(
            "id" => $actualizarDatos->id,
            "cantidad" => $actualizarDatos->cantidad,
            "descripcion" => $actualizarDatos->descripcion,
            "id_empleado" => $actualizarDatos->id_empleado,
            "id_caja" => $actualizarDatos->id_caja
        );
        return $array;

    }

    public function  eliminarMovimiento($id){
        $Elimar=Movimientos::where('id',$id)->delete();
        $array = array(
            "eliminado" => "el movimiento a sido eliminado con exito",

        );

        return $array;

    }
public  function  movimientosDia($search){
    $users = DB::table('movimientos')
        ->where('created_at' , 'LIKE' ,"%{$search}%")
        ->get();
    return $users;

}

    public  function  idCaja($search){

        $users = DB::table('movimientos')
            ->Where('id_caja','LIKE',"%{$search}%")
            ->get();
        return $users;

    }

    public  function  busquedaFecha(Request $request,$search){
      $users=  DB::table('empleados')
            ->leftJoin('movimientos', 'empleados.id', '=', 'movimientos.id_empleado')
            ->Where('movimientos.created_at','LIKE',"%{$search}%")
            ->select('empleados.nombre','movimientos.cantidad','movimientos.descripcion','movimientos.created_at')
            ->get();

        return $users;
    }


    public  function  date(Request $request, $inicio , $fin){
        $users=  DB::table('empleados')
            ->leftJoin('movimientos', 'empleados.id', '=', 'movimientos.id_empleado')
            ->where('movimientos.created_at','>=',$inicio)->where('movimientos.created_at','<=',$fin)
            ->select('empleados.nombre','movimientos.cantidad','movimientos.descripcion','movimientos.created_at')
            ->get();

        return $users;
    }

}
