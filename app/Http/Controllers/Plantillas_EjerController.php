<?php

namespace App\Http\Controllers;
use App\Plantillas_Ejer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class Plantillas_EjerController extends Controller
{
    public function store(Request $request){

        $platilla = new Plantillas_Ejer();
        $platilla->nombre =$request->input('nombre');
        $platilla->descripcion = $request->input('descripcion');
        $platilla->categoria_id = $request->input('categoria_id');
        $platilla->save();



       // $url = 'http://localhost/subir_imagenes/index.php';
       // header('Location: '.$url.'?id='.$platilla->id);
        return redirect('http://itelisoft.mx/API/holixlabs/uploads/img/index.php?id='.$platilla->id);




    }

    public function storePlantilla(Request $request){

        $data = $request->json()->all();

        $platilla = new Plantillas_Ejer();
        $platilla->nombre = $data['nombre'];
        $platilla->descripcion = $data['descripcion'];
        $platilla->categoria_id = $data['categoria_id'];
        $platilla->save();

        $array = array(
            "id"   =>  $platilla->id
        );

        return $array;
    }

    public function  EliminarPlanEjer($id){
        $Elimar=Plantillas_Ejer::where('id',$id)->delete();
        return "La plantilla a sido eliminado con éxito!";
    }



    public function consultarPlaejer($id)
    {
        $inv = Plantillas_Ejer::where('id', $id)->get();
        return response($inv);
    }




    public function consultarPlanEjercicios()
    {
        return response(Plantillas_Ejer::all());
    }




    public function  listaEjercicios($categoria_id){
        $inv = Plantillas_Ejer::where('categoria_id', $categoria_id)
            ->orderBy('nombre', 'asc')
            ->get(['id','nombre']);
        return response($inv);



    }

    public function buscarPlantilla($search)
    {
        return  $user = Plantillas_Ejer::where('nombre','LIKE',"%{$search}%")->get(['id','nombre']);
    }

    public function agregaPlantillas()
    {
        if (Auth::check()) {
            return view('plantillas_ejer.agregarEjercicio');

        }else{
            return redirect('login');
        }

    }

    public function showPlantillas()
    {
        if (Auth::check()) {
            return view('plantillas_ejer.showEjercicio');

        }else{
            return redirect('login');
        }

    }

    public function guardarFotos()
    {
        if (Auth::check()) {
            return view('plantillas_ejer.guarda');

        }else{
            return redirect('login');
        }

    }
    // -----------------------------------------------------------------------------------------------------------------

    public function consultarDatos($id)
    {
        return json_encode( Plantillas_Ejer::where('id', $id)->get(['id','nombre','descripcion','categoria_id']),JSON_UNESCAPED_UNICODE );


    }




    // -----------------------------------------------------------------------------------------------------------------



    public  function ordenAlfebetico(){
        $users = DB::table('plantillas_ejer')
            ->orderBy('nombre', 'asc')
            ->get();

        return $users;
    }

}
