<?php

namespace App\Http\Controllers;

use App\Ejercicios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ToolsController extends Controller
{

    public function  getRutinaCompletaByAlumnoId($id_alumno){

      $rutina = (array)app(\App\Http\Controllers\AlumnosController::class)->consultaAlumno($id_alumno);
        $id_rutina= (int)collect($rutina)->toArray()['original'];


                 $ejercicios = (array)app(\App\Http\Controllers\EjerciciosController::class)->consultarEjerciciosByRutinaId($id_rutina);
                 $arreglo_ejercicios= collect($ejercicios)->toArray()['original'];


              $muro = array();

                 for($i = 0; $i < count($arreglo_ejercicios); ++$i) {


                     $plantilla_ejercicio = collect(app(\App\Http\Controllers\Plantillas_EjerController::class)->consultarPlaejer($arreglo_ejercicios[$i]['plantilla_ejer_id']))->toArray()['original'][0];

                     $categoria = collect(app(\App\Http\Controllers\CategoriasController::class)->consultarCategoria($plantilla_ejercicio['categoria_id']))->toArray()['original'];



                     $post['id_ejercicio'] = $arreglo_ejercicios[$i]['id'];
                     $post['id_plantilla'] = $plantilla_ejercicio['id'];
                     $post['nombre'] = $plantilla_ejercicio['nombre'];
                     $post['descripcion'] = $plantilla_ejercicio['descripcion'];
                     $post['notas_entrenador'] = $arreglo_ejercicios[$i]['notas'];
                     $post['categoria'] = $categoria;
                     $post['url'] = 'plantilla_'.$plantilla_ejercicio['id'].'_num_1.jpg';
                     $post['dia'] = $arreglo_ejercicios[$i]['dia'];


                     array_push($muro, $post);

                 }

                 return $muro;

    }

    public function  rutinaCompleta($id_rutina){



        $ejercicios = (array)app(\App\Http\Controllers\EjerciciosController::class)->consultarEjerciciosByRutinaId($id_rutina);
        $arreglo_ejercicios= collect($ejercicios)->toArray()['original'];


        $muro = array();

        for($i = 0; $i < count($arreglo_ejercicios); ++$i) {


            $plantilla_ejercicio = collect(app(\App\Http\Controllers\Plantillas_EjerController::class)->consultarPlaejer($arreglo_ejercicios[$i]['plantilla_ejer_id']))->toArray()['original'][0];

           $categoria = collect(app(\App\Http\Controllers\CategoriasController::class)->consultarCategoria($plantilla_ejercicio['categoria_id']))->toArray()['original'];
          // print_r( $arreglo_ejercicios);
           $imagen = collect(app(\App\Http\Controllers\ImagenesController::class)->mostrarUrl($arreglo_ejercicios[$i]['plantilla_ejer_id']))->toArray();

          //  print  $imagen[0]->url;



            $post['id_ejercicio'] = $arreglo_ejercicios[$i]['id'];
            $post['id_plantilla'] = $plantilla_ejercicio['id'];
            $post['nombre'] = $plantilla_ejercicio['nombre'];
            $post['descripcion'] = $plantilla_ejercicio['descripcion'];
            $post['notas_entrenador'] = $arreglo_ejercicios[$i]['notas'];
            $post['categoria'] = $categoria;
            $post['url'] = $imagen[0]->url;
            $post['dia'] = $arreglo_ejercicios[$i]['dia'];


           array_push($muro, $post);

        }

       return $muro;

    }


public  function rutina($search){

  $ejercicios=DB::table('ejercicios')
        ->join('plantillas_ejer', 'ejercicios.plantilla_ejer_id', '=', 'plantillas_ejer.id')
        ->leftJoin('categorias', 'plantillas_ejer.categoria_id', '=', 'categorias.id')
        ->Where('ejercicios.rutina_id','LIKE',"{$search}")
        ->select('ejercicios.id as id_ejercicio','ejercicios.plantilla_ejer_id as id_plantilla','plantillas_ejer.nombre','plantillas_ejer.descripcion','ejercicios.notas as notas_entrenador','categorias.nombre as categoria','ejercicios.dia')
        ->get();


return $ejercicios;
}









}
