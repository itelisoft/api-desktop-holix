<?php

namespace App\Http\Controllers;

use App\Socios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SociosController extends Controller
{


    public function login(Request $request)
    {

        $data = $request->json()->all();

        $infocodigo = Socios::where('codigo', '=', $data['codigo'])->get();


        if (count($infocodigo) == 0) {
          
                  return "{'error' : 'contraseña incorrecta'}";

        }

        else {
            $array = array(

                "id" => $infocodigo[0]->id,
                "nombre" => $infocodigo[0]->nombre,
                "codigo" => $infocodigo[0]->codigo,
                "url" => $infocodigo[0]->url,
                "direccion" => $infocodigo[0]->direccion,
                "telefono" => $infocodigo[0]->telefono,
                "fecha_nacimiento" => $infocodigo[0]->fecha_nacimiento,
                "apodo" => $infocodigo[0]->apodo,
                "notas" => $infocodigo[0]->notas,
                "saldo" => $infocodigo[0]->saldo,
                "proximo_pago" => $infocodigo[0]->proximo_pago,
                "correo" => $infocodigo[0]->correo,
                "membresia" => $infocodigo[0]->membresia
            );

            return $array;
        }



    }



    public function storeSocios(Request $request)
    {


        $data = $request->json()->all();
        $consultaSocio = Socios::where('codigo', '=', $data['codigo'])->get();

        if (count($consultaSocio) == 0) {

            $socio = new Socios();
            $socio->nombre = $data['nombre'];
            $socio->codigo = $data['codigo'];
            $socio->url = $data['url'];
            $socio->direccion = $data['direccion'];
            $socio->telefono = $data['telefono'];
            $socio->fecha_nacimiento = $data['fecha_nacimiento'];
            $socio->apodo = $data['apodo'];
            $socio->notas = $data['notas'];
            $socio->saldo = $data['saldo'];
            $socio->proximo_pago = $data['proximo_pago'];
            $socio->correo = $data['correo'];
            $socio->membresia = $data['membresia'];
            $socio->save();

            $array = array(
                "id" => $socio->id,
                "nombre" => $socio->nombre,
                "codigo" => $socio->codigo,
                "url" => $socio->url,
                "direccion" => $socio->direccion,
                "telefono" => $socio->telefono,
                "fecha_nacimiento" => $socio->fecha_nacimiento,
                "apodo" => $socio->apodo,
                "notas" => $socio->notas,
                "saldo" => $socio->saldo,
                "proximo_pago" => $socio->proximo_pago,
                "correo" => $socio->correo,
                "membresia" => $socio->membresia
            );

            return $array;


        } else {

            return response("Error Digita otro código");
            }

    }


        public function consultarSocio($id)
        {
            $consultar = Socios::where('id', $id)->get();
            return response($consultar);
        }



    public function consultarSocios()
    {
       // return response(Socios::all());

        $users = DB::table('socios')
            ->orderBy('proximo_pago', 'asc')
            ->get();

        return $users;

    }


    public function consultaSocios()
    {
        return response(Socios::all());



    }

    public function buscarsocioporNombre($search)
    {

        $users = DB::table('socios')
            ->Where('nombre','LIKE',"%{$search}%")
            ->get();
        return $users;

    }


    public function actualizarSocio(Request $request)
    {
        $data = $request->json()->all();
        $consultaSocio =Socios::where('codigo', '=', $data['codigo'])->get();

        if(count($consultaSocio)== 0) {
            $actualizarDatos = Socios::find($data['id']);
            $actualizarDatos->nombre = $data['nombre'];
            $actualizarDatos->codigo = $data['codigo'];
            $actualizarDatos->url = $data['url'];
            $actualizarDatos->direccion = $data['direccion'];
            $actualizarDatos->telefono = $data['telefono'];
            $actualizarDatos->fecha_nacimiento = $data['fecha_nacimiento'];
            $actualizarDatos->apodo = $data['apodo'];
            $actualizarDatos->notas = $data['notas'];
            $actualizarDatos->saldo = $data['saldo'];
            $actualizarDatos->proximo_pago = $data['proximo_pago'];
            $actualizarDatos->correo = $data['correo'];
            $actualizarDatos->membresia = $data['membresia'];
            $actualizarDatos->save();


            $array = array(
                "id" => $actualizarDatos->id,
                "nombre" => $actualizarDatos->nombre,
                "codigo" => $actualizarDatos->codigo,
                "url" => $actualizarDatos->url,
                "direccion" => $actualizarDatos->direccion,
                "telefono" => $actualizarDatos->telefono,
                "fecha_nacimiento" => $actualizarDatos->fecha_nacimiento,
                "apodo" => $actualizarDatos->apodo,
                "notas" => $actualizarDatos->notas,
                "saldo" => $actualizarDatos->saldo,
                "proximo_pago" => $actualizarDatos->proximo_pago,
                "correo" => $actualizarDatos->correo,
                "membresia" => $actualizarDatos->membresia
            );
            return $array;
        }
        else{

            if($consultaSocio[0]->id ==  $data['id']  ){

                $actualizarDatos = Socios::find($data['id']);
                $actualizarDatos->nombre = $data['nombre'];
                $actualizarDatos->codigo = $data['codigo'];
                $actualizarDatos->url = $data['url'];
                $actualizarDatos->direccion = $data['direccion'];
                $actualizarDatos->telefono = $data['telefono'];
                $actualizarDatos->fecha_nacimiento = $data['fecha_nacimiento'];
                $actualizarDatos->apodo = $data['apodo'];
                $actualizarDatos->notas = $data['notas'];
                $actualizarDatos->saldo = $data['saldo'];
                $actualizarDatos->proximo_pago = $data['proximo_pago'];
                $actualizarDatos->correo = $data['correo'];
                $actualizarDatos->membresia = $data['membresia'];
                $actualizarDatos->save();

                $array = array(
                    "id" => $actualizarDatos->id,
                    "nombre" => $actualizarDatos->nombre,
                    "codigo" => $actualizarDatos->codigo,
                    "url" => $actualizarDatos->url,
                    "direccion" => $actualizarDatos->direccion,
                    "telefono" => $actualizarDatos->telefono,
                    "fecha_nacimiento" => $actualizarDatos->fecha_nacimiento,
                    "apodo" => $actualizarDatos->apodo,
                    "notas" => $actualizarDatos->notas,
                    "saldo" => $actualizarDatos->saldo,
                    "proximo_pago" => $actualizarDatos->proximo_pago,
                    "correo" => $actualizarDatos->correo,
                    "membresia" => $actualizarDatos->membresia
                );
                return $array;
            }else{


                return response("Error Digita otro código");

            }


        }

    }

    public function  eliminarSocio($id){
        $Elimar=Socios::where('id',$id)->delete();
        $array = array(
            "eliminado" => "el socio a sido eliminado con exito",

        );

        return $array;

    }

    public function buscarporCodigo($search)
    {

        $users = DB::table('socios')
            ->Where('codigo','LIKE',"%{$search}%")
            ->get();
        return $users;

    }




}
