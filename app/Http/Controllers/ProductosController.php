<?php

namespace App\Http\Controllers;

use App\Productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductosController extends Controller
{
    public function storeProductos(Request $request)
    {

        $data = $request->json()->all();

        $productos = new Productos();
        $productos->nombre = $data['nombre'];
        $productos->precio = $data['precio'];
        $productos->stock = $data['stock'];
        $productos->notas = $data['notas'];
        $productos->save();

        $array = array(
            "id" => $productos->id,
            "nombre" => $productos->nombre,
            "precio" => $productos->precio,
            "stock" => $productos->stock,
            "notas" => $productos->notas
        );
        return $array;
    }

    public function consultarProducto($id)
    {
        $consultar = Productos::where('id', $id)->get();
        return response($consultar);
    }


    public function consultarProductos()
    {
        return response(Productos::all());

    }


    public function buscarproductoporNombre($search)
    {

        $users = DB::table('productos')
            ->Where('nombre','LIKE',"%{$search}%")
            ->get();
        return $users;

    }


    public function actualizarProducto(Request $request)
    {
        $data = $request->json()->all();

        $actualizarDatos = Productos::find($data['id']);
        $actualizarDatos->nombre = $data['nombre'];
        $actualizarDatos->precio = $data['precio'];
        $actualizarDatos->stock = $data['stock'];
        $actualizarDatos->notas = $data['notas'];
        $actualizarDatos->save();

        $array = array(
            "id" => $actualizarDatos->id,
            "nombre" => $actualizarDatos->nombre,
            "precio" => $actualizarDatos->precio,
            "stock" => $actualizarDatos->stock,
            "notas" => $actualizarDatos->notas
        );
        return $array;

    }

    public function  eliminarProducto($id){
        $Elimar=Productos::where('id',$id)->delete();
        $array = array(
            "eliminado" => "el producto a sido eliminado con exito",

        );

        return $array;

    }
}
