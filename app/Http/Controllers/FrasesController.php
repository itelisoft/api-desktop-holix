<?php

namespace App\Http\Controllers;

use App\Frases;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class FrasesController extends Controller
{


    public function store(Request $request){
        $frase = new Frases();
        $frase->texto =$request->input('texto');
        $frase->save();
        flash('Los datos se guaradaron con exito!')->success();

        return redirect('view/frases/agregarFrases');

    }

    public function  EliminarFrase($id){
        $Elimar=Frases::where('id',$id)->delete();
        return "La frase a sido eliminada con éxito!";
    }



    public function consultarFrase($id)
    {
        $inv = Frases::where('id', $id)->get();
        return response($inv);
    }




    public function consultarFrases()
    {
        return response(Frases::all());
    }




    public function agregarFrases()
    {
        if (Auth::check()) {
            return view('frases.agregaFrases');

        }else{
            return redirect('login');
        }

    }

    public function showFrases()
    {
        if (Auth::check()) {
            return view('frases.showfrases');

        }else{
            return redirect('login');
        }

    }
}
