<?php

namespace App\Http\Controllers;

use App\Empleados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmpleadosController extends Controller
{

    public function loginEmpleado(Request $request)
    {

        $data = $request->json()->all();


        $infocodigo = Empleados::where('usuario', '=', $data['usuario'])->get();


        if (count($infocodigo) == 0) {

            return response("Error Código no resgistrado");

        }
        if (strcmp( $infocodigo->contrasena = $data['contrasena'] , $infocodigo[0]['contrasena'] ) != 0) {

            return response("Error Contraseña incorrecta");

        }



        $array = array(
            "id" => $infocodigo[0]->id,
            "nombre" => $infocodigo[0]->nombre,
            "usuario" => $infocodigo[0]->usuario,
            "contrasena" => $infocodigo[0]->contrasena,
            "tipo" => $infocodigo[0]->tipo,
            "remember_token" => "null",
            "created_at" => "",

            );

        return $array;
    }



    public function storeEmpleado(Request $request){

        $data = $request->json()->all();
        $consultaUsuario = Empleados::where('usuario', '=', $data['usuario'])->get();



        if(count($consultaUsuario)== 0) {


            $empleado = new Empleados();
            $empleado->nombre = $data['nombre'];
            $empleado->usuario = $data['usuario'];
            $empleado->contrasena = $data['contrasena'];
            $empleado->tipo = $data['tipo'];
            $empleado->save();





            $array = array(
                "id" => $empleado->id,
                "nombre" => $empleado->nombre,
                "usuario" => $empleado->usuario,
                "contrasena" => $empleado->contrasena,
                "tipo" => $empleado->tipo,
            );

            return $array;
        } else {


            return response("Digita otro usuario");

        }

    }

    public function  eliminarEmpleado($id){
        $Elimar=Empleados::where('id',$id)->delete();
        $array = array(
            "eliminado" => "el empleado a sido eliminado con exito",

        );

        return $array;

    }

    public function consultarEmpleado($id)
    {
        $consultar = Empleados::where('id', $id)->get();
        return response($consultar);
    }


    public function consultarEmpleados()
    {
        return response(Empleados::all());

    }


    public function buscarEmpleadoporNombre($search)
    {

        $users = DB::table('empleados')
            ->Where('nombre','LIKE',"%{$search}%")
            ->get();
        return $users;

    }

    public function buscarEmpleadoporCodigo($search)
    {

        $users = DB::table('empleados')
            ->Where('contrasena','LIKE',"%{$search}%")
            ->get();
        return $users;

    }

    public function actualizarEmpleado(Request $request)
    {

        $data = $request->json()->all();
        $consultaUsuario =Empleados::where('usuario', '=', $data['usuario'])->get();

        if(count($consultaUsuario)== 0) {

            $actualizarDatos = Empleados::find($data['id']);
            $actualizarDatos->nombre = $data['nombre'];
            $actualizarDatos->usuario = $data['usuario'];
            $actualizarDatos->contrasena = $data['contrasena'];
            $actualizarDatos->tipo = $data['tipo'];
            $actualizarDatos->save();

            $array = array(
                "id" => $actualizarDatos->id,
                "nombre" => $actualizarDatos->nombre,
                "usuario" => $actualizarDatos->usuario,
                "contrasena" => $actualizarDatos->contrasena,
                "tipo" => $actualizarDatos->tipo,
                "remember_token" => "null",
                "created_at" => "",

            );

            return $array;
        }
        else{

            if($consultaUsuario[0]->id ==  $data['id']  ){

                $actualizarDatos = Empleados::find($data['id']);
                $actualizarDatos->nombre = $data['nombre'];
                $actualizarDatos->usuario = $data['usuario'];
                $actualizarDatos->contrasena = $data['contrasena'];
                $actualizarDatos->tipo = $data['tipo'];
                $actualizarDatos->save();

                $array = array(
                    "id" => $actualizarDatos->id,
                    "nombre" => $actualizarDatos->nombre,
                    "usuario" => $actualizarDatos->usuario,
                    "contrasena" => $actualizarDatos->contrasena,
                    "tipo" => $actualizarDatos->tipo,
                    "remember_token" => "null",
                    "created_at" => "",

                );

                return $array;
            }else{

               return response("Error Digita otro usuario");
            }


        }

    }

    public function buscarIdEmpleado($search){
        $users = DB::table('empleados')
            ->Where('id','LIKE',"%{$search}%")
            ->get(['nombre']);
        return $users;

    }


}
