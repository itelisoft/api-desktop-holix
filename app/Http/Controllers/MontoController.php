<?php

namespace App\Http\Controllers;

use App\Monto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MontoController extends Controller
{
    public function storeMonto(Request $request)
    {

        $data = $request->json()->all();

        $monto = new Monto();
        $monto->cantidad = $data['cantidad'];
        $monto->tipo = $data['tipo'];
        $monto->fecha = $data['fecha'];
        $monto->save();

        $array = array(
            "id" => $monto->id,
            "cantidad" => $monto->cantidad,
            "tipo" => $monto->tipo,
            "fecha" => $monto->fecha
        );
        return $array;
    }

    public function consultarMonto($id)
    {
        $consultar = Monto::where('id', $id)->get();
        return response($consultar);
    }


    public function consultarMontos()
    {
        return response(Monto::all());

    }


    public function buscarMonto($search)
    {

        $users = DB::table('monto')
            ->Where('tipo','LIKE',"%{$search}%")
            ->get();
        return $users;

    }


    public function ActualizarMonto(Request $request)
    {
        $data = $request->json()->all();

        $actualizarDatos = Monto::find($data['id']);
        $actualizarDatos->cantidad = $data['cantidad'];
        $actualizarDatos->tipo = $data['tipo'];
        $actualizarDatos->fecha = $data['fecha'];
        $actualizarDatos->save();
        return "se actualizarón los datos del monto";

    }

    public function  eliminarMonto($id){
        $Elimar=Monto::where('id',$id)->delete();
        $array = array(
            "eliminado" => "el monto a sido eliminado con exito",

        );

        return $array;

    }


}
