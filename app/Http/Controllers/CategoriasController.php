<?php

namespace App\Http\Controllers;

use App\Categorias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CategoriasController extends Controller
{
    public function store(Request $request){

        $categoria = new Categorias();
        $categoria->nombre =$request->input('nombre');
        $categoria->descripcion = $request->input('descripcion');
        $categoria->save();

        return $categoria->id;
    }

    public function  EliminarCategoria($id){
        $Elimar=Categorias::where('id',$id)->delete();
        return "La categoría a sido eliminada con éxito!";
    }



    public function consultarCategoria($id)
    {
        $inv = Categorias::where('id', $id)->pluck('nombre')[0];  // regresa solo el nombre
        return response($inv);
    }




    public function consultarCategorias()
    {

        $users = DB::table('categorias')
            ->orderBy('nombre', 'asc')
            ->get();

        return $users;

    }

    public function busquedaPorcategoria($search)
    {
        return  $user = Categorias::where('nombre','LIKE',"%{$search}%")->get(['id','nombre']);
    }


    // -----------------------------------------------------------------------------------------------------------------

    public function consultarCTG($id)
    {
      return  $rutina = Categorias::where('id', $id)->pluck('nombre')[0];



    }


    // -----------------------------------------------------------------------------------------------------------------
}
