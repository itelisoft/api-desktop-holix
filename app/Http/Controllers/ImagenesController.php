<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imagenes;
use Illuminate\Support\Facades\DB;

class ImagenesController extends Controller
{
    public function store(Request $request){

        $gym = new Imagenes();
        $gym->plantillas_ejer_id =$request->input('plantillas_ejer_id');
        $gym->numero = $request->input('numero');
        $gym->url = $request->input('url');
        $gym->save();

        return $gym->id;
    }

    public function  EliminarImagen($id){
        $Elimar=Imagenes::where('id',$id)->delete();
        return "El usuario a sido eliminado con éxito!";
    }



    public function consultarImagen($id)
    {
        $inv = Imagenes::where('id', $id)->get();
        return response($inv);
    }




    public function consultarImagenes()
    {
        return response(Imagenes::all());
    }


    public function mostrarUrl($id)
    {
        $users = DB::table('imagenes')
            ->where('plantillas_ejer_id', $id)
            ->Where('numero', '=', 1)
            ->get();
        return $users;
    }




}
