<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rutinas;
class RutinasController extends Controller
{
    public function store(Request $request){

        $rutina = new Rutinas();
        $rutina->id_alumno =$request->input('id_alumno');
        $rutina->recomendaciones = $request->input('recomendaciones');
        $rutina->save();

        return $rutina->id;
    }


    public function  EliminarRutina($id){
        $Elimar=Rutinas::where('id',$id)->delete();
        return "El usuario a sido eliminado con éxito!";
    }



    public function consultarRutina($id)
    {
        $inv = Rutinas::where('id', $id)->get();
        return response($inv);
    }




    public function consultarRutinas()
    {
        return response(Rutinas::all());
    }




    public function consultarRutinaByIdAlumno($id_alumno)
    {
        $rutina = Rutinas::where('id_alumno', $id_alumno)->get();
        return response($rutina);

    }

    public function ActualizarDatosid_alumno($id,$id_alumno)
    {
        $actualizarDatos = Rutinas::find($id);

        $actualizarDatos ->id_alumno = $id_alumno;
        $actualizarDatos->save();
        return "se actualizarón los datos del alumno";

    }

    public function crearRutina($id_alumno)
    {
        $rutina = new Rutinas();
        $rutina->id =$id_alumno;
        $rutina->id_alumno =$id_alumno;
        $rutina->recomendaciones ='puedes realizar notas aquí';
        $rutina->save();
        return $rutina->id;

    }

    public function crearRutinaWeb($id_alumno)
    {
        $rutina = new Rutinas();
        $rutina->id =$id_alumno;
        $rutina->id_alumno =$id_alumno;
        $rutina->recomendaciones ='Registrado en web';
        $rutina->save();
        return $rutina->id;

    }

    public function consultaridRutina($id)
    {
        $inv = Rutinas::where('id', $id)->pluck('id')[0];
        return $inv;
    }

    public function consultarEjerIdrutina($id)
    {
        $rutina = Rutinas::where('id', $id)->get(['id','recomendaciones']);
        return $rutina;
    }

}
