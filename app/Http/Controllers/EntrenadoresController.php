<?php

namespace App\Http\Controllers;
use App\Entrenadores;
use Illuminate\Http\Request;

class EntrenadoresController extends Controller
{
    public function login(Request $request)
    {

        $data = $request->json()->all();

        $infocodigo = Entrenadores::where('codigo', '=', $data['codigo'])->get();


        if (count($infocodigo) == 0) {
            return "CÓDIGO NO REGISTRADO";
        }

        return  $infocodigo[0]->id." , ".$infocodigo[0]->id_gimnasio;


    }



    public function store(Request $request){

        $entrenador = new Entrenadores();
        $entrenador->nombre =$request->input('nombre');
        $entrenador->foto = $request->input('foto');
        $entrenador->codigo =$request->input('codigo');
        $entrenador->descripcion = $request->input('descripcion');
        $entrenador->id_gimnasio = $request->input('id_gimnasio');
        $entrenador->save();

      //  return $entrenador->nombre;

        $array = array(
            "nombre"   =>  $entrenador->nombre
        );

        return $array;
    }


    public function storeEntrenador(Request $request)
    {

        $data = $request->json()->all();
        $consultaCodigo = Entrenadores::where('codigo', '=', $data['codigo'])->get();


        if(count($consultaCodigo) == 0){
        $entrenador = new Entrenadores();
        $entrenador->nombre = $data['nombre'];
        $entrenador->foto = $data['foto'];
        $entrenador->codigo = $data['codigo'];
        $entrenador->descripcion = $data['descripcion'];
        $entrenador->id_gimnasio = $data['id_gimnasio'];
        $entrenador->save();

        $array = array(
            "nombre" => $entrenador->nombre
        );

        return $array;
    }
     else {


        print 'Digita otro código';

    }
    }


    public function  EliminarEntrenador($id){
        $Elimar=Entrenadores::where('id',$id)->delete();
        return "El usuario a sido eliminado con éxito!";
    }



    public function consultarEntrenador($id)
    {
        $inv = Entrenadores::where('id', $id)->get();
        return response($inv);
    }




    public function consultarEntrenadores()
    {
        return response(Entrenadores::all());
    }


    public function consultarDatosEntrenador($id)
    {
        $inv = Entrenadores::where('id', $id)->get(['nombre','foto','descripcion']);
        return response($inv);
    }





}
