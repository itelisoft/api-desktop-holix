<?php

namespace App\Http\Controllers;

use App\Accesogym;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccesogymController extends Controller
{


    public function store(Request $request)
    {

        $data = $request->json()->all();

        $acceso = new Accesogym();
        $acceso->acceso = $data['acceso'];
        $acceso->estado_puerta = $data['estado_puerta'];
        $acceso->nombre_socio = $data['nombre_socio'];
        $acceso->token = $data['token'];
        $acceso->save();

        $array = array(
            "id" => $acceso->id,
            "acceso" => $acceso->acceso,
            "estado_puerta" => $acceso->estado_puerta,
            "nombre_socio" => $acceso->nombre_socio,
            "token" => $acceso->token
        );
        return $array;
    }


    public function consulta($id)
    {
        $consultar = Accesogym::where('id', $id)->get();
        return response($consultar);
    }


    public function consultar()
    {
        return response(Accesogym::all());

    }


    public function buscar($search)
    {

        $users = DB::table('accesogym')
            ->Where('acceso','LIKE',"%{$search}%")
            ->get();
        return $users;

    }


    public function actualizar(Request $request)
    {
        $data = $request->json()->all();

        $actualizarDatos = Accesogym::find($data['id']);
        $actualizarDatos->acceso = $data['acceso'];
        $actualizarDatos->estado_puerta = $data['estado_puerta'];
        $actualizarDatos->nombre_socio = $data['nombre_socio'];
        $actualizarDatos->token = $data['token'];
        $actualizarDatos->save();
        $array = array(
            "id" => $actualizarDatos->id,
            "acceso" => $actualizarDatos->acceso,
            "estado_puerta" => $actualizarDatos->estado_puerta,
            "nombre_socio" => $actualizarDatos->nombre_socio,
            "token" => $actualizarDatos->token
        );
        return $array;
    }

    public function  eliminar($id){
        $Elimar=Accesogym::where('id',$id)->delete();
        $array = array(
            "eliminado" => "el acceso al gym  a sido eliminado con exito",

        );

        return $array;

    }
}
