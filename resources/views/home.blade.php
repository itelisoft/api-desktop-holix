@extends('layouts.app')

@section('content')
<br>
<div class="container">



    <div class="card-deck">

        <div class="card">
            <img class="card-img-top" src="../resources/imag_holix_01.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Gimnasios</h5>
                <p class="card-text">En este apartado podrás agregar , editar o eliminar información de los gimnasios.</p>



                <div class="btn-group btn-group-justified">
                    <a href="{{ url('view/gimnasios/agregaGym') }}" class="btn btn-primary" style="width: 150px;height:100%;  margin: 2px">Agregar</a>
                    <a href="{{ url('view/gimnasios/ShowGym') }}" class="btn btn-primary" style="width: 160px;height:100%;  margin: 2px">Editar / Eliminar</a>
                </div>
            </div>
        </div>

        <div class="card">
            <img class="card-img-top" src="../resources/imag_holix_02.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Ejercicios</h5>
                <p class="card-text">En este apartado podrás agregar , editar o eliminar información de los ejercicios.</p>


                <div class="btn-group btn-group-justified">
                    <a href="{{ url('view/platilla_ejer/agregaPlantillas') }}" class="btn btn-primary" style="width: 150px;height:100%;  margin: 2px">Agregar</a>
                    <a href="{{ url('show/platilla_ejer/showPlantillas') }}" class="btn btn-primary" style="width: 160px;height:100%;  margin: 2px">Editar / Eliminar</a>
                </div>
            </div>
        </div>

        <div class="card">
            <img class="card-img-top" src="../resources/imag_holix_03.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Frases</h5>
                <p class="card-text">En este apartado podrás agregar , editar o eliminar información de las frases.</p>
                <div class="btn-group btn-group-justified">
                <a href="{{ url('view/frases/agregarFrases') }}" class="btn btn-primary" style="width: 150px;height:100%;  margin: 2px ">Agregar</a>
                    <br>
                <a href="{{ url('view/frases/showFrases') }}" class="btn btn-primary" style="width: 160px;height:100%;  margin: 2px">Editar / Eliminar</a>
                </div>


            </div>
        </div>

    </div>





</div>

@endsection
