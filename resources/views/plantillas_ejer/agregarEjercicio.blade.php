@extends("layouts.app")
@section("content")
    <div class="container">
        <br>


        <div class="row justify-content-center">




            <form method="POST" class="col-md-8" action="./store">
                {{csrf_field()}}
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">

                <div class="form-row">
                    <label for="nombre">Nombre </label>
                    <input type="text" required class="form-control" id="nombre" name="nombre" placeholder="Nombre del ejercicio">
                </div>
                <br>
                <div class="form-row">

                    <label for="descripcion">Descripción</label>


                    <textarea name="descripcion" required class="form-control" id="descripcion" rows="5" cols="68"  >Escribe aquí tus comentarios</textarea>

                </div>
                <br>
                <div class="form-row">


                    <label for="categoria_id">categoria_id</label>
                    <input type="text"  required  class="form-control" id="categoria_id" name="categoria_id" placeholder="Número de categoría">
                </div>
                <br>

                <div class="form-row">

                    <button type="submit"  class="btn btn-primary" >Continuar</button>
                </div>

                        </div>
                    </div>
                </div>

            </form>





</div>



    </div>

@endsection