@extends("layouts.app")
@section("content")
    <div class="container">
        <br>
        <div class="row justify-content-center">




        <form method="POST" class="col-md-8" action="./store">
            {{csrf_field()}}
            <div class="card-deck">
                <div class="card">
                    <div class="card-body">

            <div class="form-row">
                    <label for="nombre">Nombre </label>
                    <input type="text" required class="form-control" id="nombre" name="nombre" placeholder="Nombre del Gym">
            </div>
            <br>
            <div class="form-row">

                    <label for="codigo">Código</label>
                    <input type="text" required class="form-control" id="codigo" name="codigo" placeholder="Código">

            </div>
            <br>
            <div class="form-row">
                <label for="logo">Logotipo</label>

                <input type="url" required  class="form-control" id="logo" name="logo" placeholder="Url del logotipo">
            </div>
            <br>

            <div class="form-row">

            <button type="submit" class="btn btn-primary" >Continuar</button>
            </div>
                    </div>
                </div>
            </div>
        </form>



</div>







    </div>
@endsection