<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



// Rutas tabla alumno
Route::post('api/alumnos/login','AlumnosController@login');
Route::post('api/alumnos/store','AlumnosController@store');
Route::get('api/alumno/Eliminaralumno/{id}','AlumnosController@Eliminaralumno');
Route::get('api/alumno/consultarAlumno/{id}','AlumnosController@consultarAlumno');
Route::get('api/alumno/consultarAlumnos','AlumnosController@consultarAlumnos');
Route::get('api/alumno/buscar_id_rutina/{id}','AlumnosController@buscar_id_rutina');
Route::get('api/alumno/ActualizarDatosAlumno/{id}/{nombre}/{edad}/{codigo}/{estatura}/{complexion}/{peso}/{notas}','AlumnosController@ActualizarDatosAlumno');
Route::get('api/alumno/BuscarAlumno','AlumnosController@BuscarAlumno');
Route::get('api/alumno/ConsultarEntrenadorAlumno/{id}','AlumnosController@ConsultarEntrenadorAlumno');
Route::get('api/alumno/alumnotieneEntrenador/{id}','AlumnosController@alumnotieneEntrenador');
Route::get('api/alumno/RutinaCompletaAlumno/{id_alumno}','AlumnosController@RutinaCompletaAlumno');
Route::get('api/alumno/buscarAlumnoporNombre/{id_entrenador}/{nombre}','AlumnosController@buscarAlumnoporNombre');
Route::get('api/alumno/buscarAlumnoporCodigo/{id}/{codigo}','AlumnosController@buscarAlumnoporCodigo');
Route::get('api/alumno/ListarAlumnos/{id}','AlumnosController@ListarAlumnos');
Route::post('api/alumno/storeAlumno','AlumnosController@storeAlumno');
Route::post('api/alumno/ActualizarAlumno','AlumnosController@ActualizarAlumno');
Route::get('api/alumno/buscaAlumosDeEntrenador/{buscar}','AlumnosController@buscaAlumosDeEntrenador');
Route::get('api/alumno/ordenAlfebetico','AlumnosController@ordenAlfebetico');




//Route::get('api/alumno/consultaAlumno/{id}','AlumnosController@consultaAlumno');

// rutas para consultar toda la rutina del alumno
Route::get('api/ejercicios/consultarEjer/{id}','EjerciciosController@consultarEjer');
Route::get('api/ejercicios/consultaridplantilla/{id}','EjerciciosController@consultaridplantilla');
Route::get('api/platilla_ejer/consultarDatos/{buscar}','Plantillas_EjerController@consultarDatos');
Route::get('api/platilla_ejer/consultaridplantilla/{buscar}','Plantillas_EjerController@consultaridplantilla');
Route::get('api/categorias/consultarCTG/{id}','CategoriasController@consultarCTG');
Route::get('api/ejercicios/consultarCategoria/{id}','EjerciciosController@consultarCategoria');
Route::get('api/platilla_ejer/contador','Plantillas_EjerController@contador');
Route::get('api/ejercicios/contador/{id}','EjerciciosController@contador');

Route::get('api/imagen/mostrarUrl/{id}','ImagenesController@mostrarUrl');


// Rutas tabla categorias
Route::post('api/categorias/store','CategoriasController@store');
Route::get('api/categorias/EliminarCategoria/{id}','CategoriasController@EliminarCategoria');
Route::get('api/categorias/consultarCategoria/{id}','CategoriasController@consultarCategoria');
Route::get('api/categorias/consultarCategorias','CategoriasController@consultarCategorias');
Route::get('api/categorias/busquedaPorcategoria/{nombre}','CategoriasController@busquedaPorcategoria');


// Rutas tabla Ejercicios
Route::post('api/ejercicios/store','EjerciciosController@store');
Route::get('api/ejercicios/eliminarEjercicio/{id}','EjerciciosController@eliminarEjercicio');
Route::get('api/ejercicios/consultarEjercicio/{id}','EjerciciosController@consultarEjercicio');
Route::get('api/ejercicios/consultarEjercicios','EjerciciosController@consultarEjercicios');
Route::get('api/ejercicios/consultarEjerciciosByRutinaId/{rutina_id}','EjerciciosController@consultarEjerciciosByRutinaId');
Route::get('api/ejercicios/actualizarid_ejercicio_id_rutina/{id}/{plantilla_ejer_id}/{rutina_id}','EjerciciosController@actualizarid_ejercicio_id_rutina');

// Rutas tabla Entrenadores
Route::post('api/entrenador/login','EntrenadoresController@login');
Route::post('api/entrenadores/store','EntrenadoresController@store');
Route::get('api/entrenador/EliminarEntrenador/{id}','EntrenadoresController@EliminarEntrenador');
Route::get('api/entrenador/consultarEntrenador/{id}','EntrenadoresController@consultarEntrenador');
Route::get('api/entrenador/consultarEntrenadores','EntrenadoresController@consultarEntrenadores');
Route::get('api/entrenador/consultarDatosEntrenador/{id}','EntrenadoresController@consultarDatosEntrenador');
Route::post('api/entrenadores/storeEntrenador','EntrenadoresController@storeEntrenador');

// Rutas tabla Frases
Route::post('view/frases/store','FrasesController@store');
Route::get('api/frases/EliminarFrase/{id}','FrasesController@EliminarFrase');
Route::get('api/frases/consultarFrase/{id}','FrasesController@consultarFrase');
Route::get('api/frases/consultarFrases','FrasesController@consultarFrases');



Route::get('view/frases/agregarFrases','FrasesController@agregarFrases');
Route::get('view/frases/showFrases','FrasesController@showFrases');

// Rutas tabla Gimnasios
Route::post('api/gimnasios/login','GimnasiosController@login');
Route::post('view/gimnasios/store','GimnasiosController@store');
Route::get('api/gimnasios/EliminarGym/{id}','GimnasiosController@EliminarGym');
Route::get('api/gimnasios/consultarGym/{id}','GimnasiosController@consultarGym');
Route::get('api/gimnasios/consultarGyms','GimnasiosController@consultarGyms');

// vistas de la tabla gimnasios

Route::get('view/gimnasios/agregaGym','GimnasiosController@agregaGym');
Route::get('view/gimnasios/ShowGym','GimnasiosController@ShowGym');

// Rutas tabla Imagen
Route::post('api/imagen/store','ImagenesController@store');
Route::get('api/imagen/EliminarImagen/{id}','ImagenesController@EliminarImagen');
Route::get('api/imagen/consultarImagen/{id}','ImagenesController@consultarImagen');
Route::get('api/imagen/consultarImagenes','ImagenesController@consultarImagenes');


// Rutas tabla Plantilla Ejercicio

Route::post('view/platilla_ejer/store','Plantillas_EjerController@store');
Route::get('api/platilla_ejer/EliminarPlanEjer/{id}','Plantillas_EjerController@EliminarPlanEjer');
Route::get('api/platilla_ejer/consultarPlaejer/{id}','Plantillas_EjerController@consultarPlaejer');
Route::get('api/platilla_ejer/consultarPlanEjercicios','Plantillas_EjerController@consultarPlanEjercicios');
Route::get('api/platilla_ejer/listaEjercicios/{categoria_id}','Plantillas_EjerController@listaEjercicios');
Route::get('api/platilla_ejer/buscarPlantilla/{buscar}','Plantillas_EjerController@buscarPlantilla');

Route::get('view/platilla_ejer/agregaPlantillas','Plantillas_EjerController@agregaPlantillas');
Route::get('show/platilla_ejer/showPlantillas','Plantillas_EjerController@showPlantillas');
Route::post('api/platilla_ejer/storePlantilla','Plantillas_EjerController@storePlantilla');



Route::get('api/platilla_ejer/ordenAlfebetico','Plantillas_EjerController@ordenAlfebetico');

// Rutas tabla Rutinas

Route::post('api/rutina/store','RutinasController@store');
Route::get('api/rutina/EliminarRutina/{id}','RutinasController@EliminarRutina');
Route::get('api/rutina/consultarRutina/{id}','RutinasController@consultarRutina');
Route::get('api/rutina/consultarRutinas','RutinasController@consultarRutinas');
Route::get('api/rutina/consultarRutinaByIdAlumno/{alumno_id}','RutinasController@consultarRutinaByIdAlumno');
Route::get('api/rutina/ActualizarDatosid_alumno/{id}/{id_alumno}','RutinasController@ActualizarDatosid_alumno');
Route::get('api/rutina/crearRutina/{id}','RutinasController@crearRutina');


// Rutas Tools Controller

Route::get('api/tools/getRutinaCompletaByAlumnoId/{id_alumno}','ToolsController@getRutinaCompletaByAlumnoId');
Route::get('api/tools/rutinaCompleta/{id_alumno}','ToolsController@rutinaCompleta');


Route::get('api/tools/rutina/{id_rutina}','ToolsController@rutina');




// rutas angular




Route::post('api/alumnos/storeJsonHombre/{h}','AlumnosController@storeJsonHombre');
Route::post('api/alumnos/storeJsonMujer/{m}','AlumnosController@storeJsonMujer');


Route::post('api/ejercicio/storeEjercicio/{$id}','EjerciciosController@storeEjercicio');


// Controlador mensaje
Route::get('mensaje/{nombre}/{correo}/{codigo}','MensajeController@mensaje');




Route::get('mensajePrueba/{nombre}/{correo}/{codigo}','MensajeController@mensajePrueba');


Route::get('mensajeUsuario/{correo}/{codigo}','MensajeController@mensajeUsuario');


// rutas para buscar alumnos sin rutina


Route::get('api/alumno/fechas/{inicio}/{fin}','AlumnosController@fechas');


Route::get('api/rutina/crearRutinaWeb/{id}','RutinasController@crearRutinaWeb');